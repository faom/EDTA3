package menunodos;
import java.io.*;
import javax.swing.JOptionPane;


class CNodo {
	int dato;
	CNodo siguiente;
        
	public CNodo()	{
            siguiente = null;
	}
}

class CLista {
    
    CNodo cabeza;
    
    public CLista()
    {
            cabeza = null;
    }

    public void InsertarDato(int dat) {
        CNodo NuevoNodo;
        CNodo antes, luego;
        NuevoNodo = new CNodo();
        NuevoNodo.dato=dat;
        int ban=0;
        if (cabeza == null){ //lista esta vacia
            NuevoNodo.siguiente=null;// ya esta hecho 
            cabeza = NuevoNodo;
        }
        else {
            if (dat<cabeza.dato) //dato va antes de cabeza
            {
                    NuevoNodo.siguiente=cabeza;
                    cabeza = NuevoNodo;
            }
            
                else {  // dato que ingresa se va con el 
                        antes=cabeza;
                        luego=cabeza;
                        while (ban==0)
                        {
                            if (dat>=luego.dato) 
                            {
                                antes=luego;
                                luego=luego.siguiente;
                            }
                            if (luego==null)
                            {
                                ban=1;
                            }
                            else 
                            {
                                    if (dat<luego.dato){
                                        ban=1;
                                    }
                            }
                        }
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=luego;
                }
        }
    }

    public void EliminarDato(int dat) {
        CNodo antes,luego;
        int ban=0;
        if (Vacia()) {
            System.out.print("Lista vacía ");
        }
        else {  if (dat<cabeza.dato) {
                    System.out.print("dato no existe en la lista ");
                }
                else {
                        if (dat==cabeza.dato) {
                            cabeza=cabeza.siguiente;
                        }
                        else {  antes=cabeza;
                                luego=cabeza;
                                while (ban==0) {
                                    if (dat>luego.dato) {
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;
                                    if (luego==null) {
                                        ban=1;
                                    }
                                    else {
                                            if (luego.dato==dat) 
                                                ban=1;
                                    }
                                }
                                if (luego==null) {
                                    System.out.print("dato no existe en la Lista ");
                                }
                                else {
                                        if (dat==luego.dato) {
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else 
                                            System.out.print("dato no existe en la Lista ");
                                }
                        }
                }
        }
    }
// si esta vacia retorna true
    public boolean Vacia() {
        return(cabeza==null);
    }

    public void Imprimir() {
        CNodo Temporal;
        Temporal=cabeza;
        if (!Vacia()) {
            while (Temporal!=null) {
                System.out.print(" " + Temporal.dato +" ");
                Temporal = Temporal.siguiente;
            }
            System.out.println("");
        }
        else
            System.out.print("Lista vacía");
    }
}
public class TrabajoListaEnlazadas {

    public static void main(String[] args) {
        int entrada;
        CLista oblista = new CLista();
        do {

            entrada= Integer.parseInt(JOptionPane.showInputDialog("-----------------MENU--------------- "
                                    + "\n1.INGRESAR \n" + "2.IMPRIMIR \n" + "3.ELIMINAR \n" + "4.SALIR"));
            switch (entrada) {
                case 1:
                   entrada=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa el Elemento"));
                   if (entrada < 0){
                    oblista.InsertarDato(entrada);
                   }else JOptionPane.showMessageDialog(null, " No se permite ingresar numero positivo");
                    break;
                case 2: 
                   oblista.Imprimir();
                   
                    break;
                case 3: entrada=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa el elemento que desee eliminar"));
                 oblista.EliminarDato(entrada);
                    break;
               case 4: System.exit(0);
                    break;
            }
        }while (entrada != 7);
             }

        }


